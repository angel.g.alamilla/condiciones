import { Component, OnInit } from '@angular/core';
import { User } from './user';
import { UserService } from './user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html'
})
export class UserComponent implements OnInit {

  users: User[];

  constructor(private userService: UserService) {  }

  ngOnInit() {
    this.userService.getUsers().subscribe(
      (users) => this.users = users
    );
  }

}
