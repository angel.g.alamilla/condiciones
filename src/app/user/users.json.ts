import { User } from './user';

export const USERS: User[] =
[
  {ap: 'qk', userName: 'Carlos Callejar', address: 'Ejercito 366', email: 'carlos.callejas@qacg.com'},
  {ap: 'pt', userName: 'Karina Hernández', address: 'Sin domicilio', email: 'karina.hp@prutech.com.mx'},
  {ap: 'px', userName: 'Blanca Buccio', address: 'Liverpool 228', email: 'bucb@praxis.com.mx'},
  {ap: 'sk', userName: 'Laura Rubio', address: 'Av. Rodolfo Gaona 3, Periodista', email: 'laura.rubio@softtek.com'},
  {ap: 'sl', userName: 'Gilberto Guzman', address: 'Sin Domicilio', email: 'gguzman@solinte.com.mx'},
  {ap: 'pt', userName: 'Francisco Gutierrez', address: 'Sin domicilio', email: 'franciso.gu@prutech.com.mx'},
  {ap: 'cap', userName: 'Ulises Torres', address: 'Tamarindos 1234', email: 'ulises.torres@capgemini.com'}
]
