import { Component, OnInit } from '@angular/core';
import { Tipo } from './tipo';
import { Condicion } from './condicion';
import { CondicionService } from './condicion.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { CondiEspecComponent } from './condi-espec.component';
import { CondiEspecEditComponent } from './condi-espec-edit.component';
import { CondiParamComponent } from './condi-param.component';
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-condespe',
  templateUrl: './cond-espe.component.html'
})
export class CondEspeComponent implements OnInit{

  private condicion: Condicion = new Condicion();

  private tipo: Tipo = null;

  tipos: Tipo[];
  errores: string[];
  titulo: string = "Nueva Condición";
  panelOpenState = false;
  dataSource: Condicion[];
  columns: any[] = [
  { label: 'ID', columnDef: 'id' },
  { label: 'Id Condición', columnDef: 'idGenerado' },
  { label: 'Descripción', columnDef: 'descripcion' },
  { label: 'estatus', columnDef: 'estatus' },
  { label: 'texto', columnDef: 'texto' },
  { label: 'Tipo / Titulo', columnDef: 'titulo' },
  { label: 'Editar', columnDef: 'editar' },
  { label: 'Parámetros', columnDef: 'param' }
  ];

  displayedColumns: string[] = this.columns.map(c => c.columnDef);

  // displayedColumns: string[] = ['ID', 'idCondicion', 'descripcion','estatus', 'texto', 'tipo', 'titulo', 'editar'];

  constructor(private condicionService: CondicionService
            , private router: Router
            , private activatedRoute: ActivatedRoute
            , public dialog: MatDialog){ }

  ngOnInit(){
    this.condicionService.getTipos().subscribe(
      (tipos) => this.tipos = tipos
    );

    this.condicionService.getCondiciones().subscribe(
      (condiciones) => this.dataSource = condiciones
    );

  }

  onOptionsSelected(event){
      this.condicion.idGenerado = event.target.value;
  }

  condicionDialog( condicion: Condicion ): void{
    condicion = new Condicion();
    condicion.descripcion = "Default";
    condicion.titulo = "Titulo Default";
    condicion.idGenerado = "#";

    const dialogRef =  this.dialog.open(CondiEspecComponent, {
      width: '750px',
      height: '420px',
      data: { condicion: Condicion }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

      this.condicionService.getCondiciones().subscribe(
        (condiciones) => this.dataSource = condiciones
      );

    });

  }

  edicionDialog( edicionCondicion: Condicion ): void{
    console.log( "Condición en edición: ", edicionCondicion );
    const dialogRef =  this.dialog.open(CondiEspecEditComponent, {
      width: '900px',
      height: '550px',
      data: edicionCondicion
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

      this.condicionService.getCondiciones().subscribe(
        (condiciones) => this.dataSource = condiciones
      );

    });

  }


    paramDialog( condicion: Condicion ): void{
      console.log( "Condición en edición: ", condicion );
      const dialogRef =  this.dialog.open(CondiParamComponent, {
        width: '850px',
        height: '520px',
        data: condicion
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');

        this.condicionService.getCondiciones().subscribe(
          (condiciones) => this.dataSource = condiciones
        );

      });

    }

  compararTipo(o1: Tipo, o2: Tipo): boolean {
    if (o1 === undefined && o2 === undefined) {
      return true;
    }

    return o1 === null || o2 === null || o1 === undefined || o2 === undefined ? false : o1.id === o2.id;
  }
}
