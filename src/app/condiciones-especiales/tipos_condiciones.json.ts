import { Tipo } from './tipo';

export const TIPOS_CONDICIONES: Tipo[] =
[
  {id: 1, nombre: 'Coberturas'},
  {id: 2, nombre: 'Servicios'},
  {id: 3, nombre: 'Alertas'},
  {id: 4, nombre: 'Tiempo de arribo'}
]
