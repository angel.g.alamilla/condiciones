import { Component, OnInit, Inject, Input } from '@angular/core';
import {CdkTextareaAutosize} from '@angular/cdk/text-field';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import swal from 'sweetalert2';

import { Condicion } from './condicion';
import { Tipo } from './tipo';
import { CondicionService } from './condicion.service';

@Component({
  selector: 'app-condi-espec-edit',
  templateUrl: './condi-espec-edit.component.html',
  styleUrls: ['./condi-espec-edit.component.css']
})
export class CondiEspecEditComponent implements OnInit {
  private opcionCond: string = "";
  forma: FormGroup;
  tipos: Tipo[];
  errores: string[];
  condicion: Condicion;
  idGenera: string = "";


  constructor(
    public formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<CondiEspecEditComponent>,
    @Inject(MAT_DIALOG_DATA) public editCondicion: Condicion,
    private condicionService: CondicionService,
    private router: Router) {

      console.log("data en constructor de edit: ", editCondicion);

      this.forma = formBuilder.group({
        'titulo': editCondicion.titulo,
        'descripcion' : editCondicion.descripcion,
        'idGenerado': editCondicion.idGenerado,
        'tipos' : this.tipos,
        'id': editCondicion.id
      });

      this.idGenera = this.editCondicion.idGenerado;

      console.log("Valor de data en constructor: ",editCondicion)

    }

  ngOnInit() {

    console.log("valor de this.data: " , this.editCondicion);

    this.condicionService.getTipos().subscribe(
      (tipos) => this.tipos = tipos
    );
  }

  formControl = new FormControl('',
  [ Validators.required ]);

  getErrorMessage() {
      return this.formControl.hasError('required') ? 'Campo requerido' :
        this.formControl.hasError('titulo') ? 'Tipo / titulo es requerido' :
        this.formControl.hasError('desripcion') ? 'Descripción es requerido' :
          '';
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onOptionsSelected(event){
      this.editCondicion.idGenerado = event.target.value;
      this.editCondicion.idGenerado = this.opcionCond;
      this.editCondicion.tipo = this.opcionCond;
      console.log("Opción seleccionada: " + this.editCondicion.idGenerado);
  }

  update(): void {

    const formaValores = this.forma.controls;

    console.log(this.forma.value);
    console.log("en create() ID Generado: " + this.idGenera);

    this.condicion = new Condicion();
    this.condicion.id = formaValores.id.value;
    this.condicion.idGenerado = this.idGenera;
    this.condicion.descripcion = formaValores.descripcion.value;
    this.condicion.titulo = formaValores.titulo.value;
    this.condicion.tipo = formaValores.titulo.value;
    this.condicion.texto = "negritas";

    console.log(this.condicion);

    this.condicionService.update(this.condicion)
      .subscribe(
        condicion => {
          this.router.navigate(['/condiciones-especiales']);
          swal('Actualización de condición', `La condición con ID: ${this.editCondicion.idGenerado} ha sido actualizada con éxito`, 'success');
          console.log(this.editCondicion);
          this.dialogRef.close();
        },
        err => {
          this.errores = err.error.errors as string[];
          console.error('Código del error desde el backend: ' + err.status);
          console.error(err.error.errors);
        }
      );
  }

  getIdGenerado(event): void{
      this.condicionService.getIdGenerado(event.nombre).subscribe(
        data => {
                        console.log("Valor de event: " + event.nombre);
                        this.idGenera = data.dataApi;
                        console.log("Valor regresado: " + data.dataApi);
                      }
      );
  }


}
