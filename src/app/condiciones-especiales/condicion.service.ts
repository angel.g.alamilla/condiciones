import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpRequest, HttpEvent } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Tipo } from './tipo';
import { Condicion } from './condicion';
import { TIPOS_CONDICIONES } from './tipos_condiciones.json';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class CondicionService{
  private urlEndPoint: string = 'http://localhost:8080/cond_api/api/insertCondicion';
  private urlEndPointApi: string = 'http://localhost:8080/cond_api/api';

  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient, private router: Router){ }

  create(condicion: Condicion): Observable<Condicion>{
    return this.http.post<Condicion>(this.urlEndPoint, condicion, { headers: this.httpHeaders })
      .pipe(
        map((response: any) => response.condicion as Condicion),
        catchError(e => {
          if (e.status == 400) {
            return throwError(e);
          }

          console.error(e.error.mensaje);
          swal(e.error.mensaje, e.error.error, 'error');
          return throwError(e);
        })
      );
  }

  update(condicion: Condicion): Observable<Condicion>{
    return this.http.post<Condicion>(this.urlEndPointApi + '/editarCondicion/' + condicion.id, condicion, { headers: this.httpHeaders })
      .pipe(
        map((response: any) => response.condicion as Condicion),
        catchError(e => {
          if (e.status == 400) {
            return throwError(e);
          }

          console.error(e.error.mensaje);
          swal(e.error.mensaje, e.error.error, 'error');
          return throwError(e);
        })
      );
  }

  getIdGenerado(tipo: string): Observable<any>{
    return this.http.get(this.urlEndPointApi + '/idProvisional/' + tipo).pipe(
      map((response: string) => response as string)
    )
  }

  getCondiciones(): Observable<Condicion[]>{
    console.log('get condiciones');
    return this.http.get(this.urlEndPointApi + '/getCondiciones').pipe(
      map(response => response as Condicion[])
    );
  }

  getTipos(): Observable<Tipo[]>{
    return of(TIPOS_CONDICIONES);
  }
}
