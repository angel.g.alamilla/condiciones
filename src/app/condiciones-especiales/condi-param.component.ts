import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { Condicion } from './condicion';

@Component({
  selector: 'app-condi-param',
  templateUrl: './condi-param.component.html',
  styleUrls: ['./condi-param.component.css']
})
export class CondiParamComponent implements OnInit {

  forma: FormGroup;

  constructor(public formBuilder: FormBuilder,
      public dialogRef: MatDialogRef<CondiParamComponent>,
      @Inject(MAT_DIALOG_DATA) public condicion: Condicion) {

        this.forma = formBuilder.group({
          'texto': this.condicion.texto
        });

      }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
