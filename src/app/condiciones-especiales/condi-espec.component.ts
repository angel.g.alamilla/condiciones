import { Component, OnInit, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import swal from 'sweetalert2';

import { Condicion } from './condicion';
import { Tipo } from './tipo';
import { CondicionService } from './condicion.service';

@Component({
  selector: 'app-condi-espec',
  templateUrl: './condi-espec.component.html',
  styleUrls: ['./condi-espec.component.css']
})
export class CondiEspecComponent implements OnInit {
  private opcionCond: string = "";
  forma: FormGroup;
  tipos: Tipo[];
  errores: string[];
  condicion: Condicion;
  idGenera: string = "";

  // constructor(public dialog: MatDialog) { }
  constructor(
    public formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<CondiEspecComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Condicion,
    private condicionService: CondicionService,
    private router: Router) {

      this.forma = formBuilder.group({
        'titulo': data.titulo,
        'descripcion' : data.descripcion,
        'idGenerado': data.idGenerado,
        'tipos' : this.tipos
      });

      console.log("Valor de data en constructor: ",this.data)

    }

  ngOnInit() {

    console.log("valor de this.data: " , this.data);

    this.condicionService.getTipos().subscribe(
      (tipos) => this.tipos = tipos
    );
  }

  create(): void {

    const formaValores = this.forma.controls;

    console.log(this.forma.value);
    console.log(formaValores.idGenerado.value.nombre);

    this.condicion = new Condicion();
    this.condicion.idGenerado = this.idGenera;
    this.condicion.descripcion = formaValores.descripcion.value;
    this.condicion.titulo = formaValores.titulo.value;
    this.condicion.tipo = formaValores.titulo.value;
    this.condicion.texto = "negritas";

    console.log(this.condicion);

    this.condicionService.create(this.condicion)
      .subscribe(
        condicion => {
          this.router.navigate(['/condiciones-especiales']);
          swal('Nueva condición', `La condición con ID: ${this.data.idGenerado} ha sido creado con éxito`, 'success');
          console.log(this.data);
          this.dialogRef.close();
        },
        err => {
          this.errores = err.error.errors as string[];
          console.error('Código del error desde el backend: ' + err.status);
          console.error(err.error.errors);
        }
      );
  }

  getIdGenerado(event): void{
      this.condicionService.getIdGenerado(event.nombre).subscribe(
        data => {
                        console.log("Valor de event: " + event.nombre);
                        this.idGenera = data.dataApi;
                        console.log("Valor regresado: " + data.dataApi);
                      }
      );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onOptionsSelected(event){
      this.data.idGenerado = event.target.value;
      this.data.idGenerado = this.opcionCond;
      this.data.tipo = this.opcionCond;
      console.log("Opción seleccionada: " + this.data.idGenerado);
  }

}
