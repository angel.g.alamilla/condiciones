import { Condicion } from './Condicion';

export const CONDICIONES: Condicion[] = [
	{
		id : 1,
		descripcion : 'UEUEUEU',
		estatus : 1,
		idGenerado : 'Tiem00010000',
		texto : 'negritas',
		tipo : 'Tiempo de arribo',
		'titulo' : 'YRYRY'
	},
	{
		id : 2,
		descripcion : 'UUUUUUU',
		estatus : 1,
		idGenerado : 'TIEM00020101',
		texto : 'negritas',
		tipo : 'Tiempo de arribo',
		titulo : 'UUUUUUU'
	},
	{
		id : 3,
		descripcion : 'PPPPPPPPPPP',
		estatus : 1,
		idGenerado : 'TIEM00020101',
		texto : 'negritas',
		tipo : 'Tiempo de arribo',
		titulo : 'OOOOOO'
	},
	{
		id : 4,
		descripcion : 'HHHHHHH',
		estatus : 1,
		idGenerado : 'TIEM00020101',
		texto : 'negritas',
		tipo : 'Tiempo de arribo',
		titulo : 'EEEEEEEE'
	},
	{
		id : 5,
		descripcion : 'JKKKJ',
		estatus : 1,
		idGenerado : 'Serv00010000',
		texto : 'negritas',
		tipo : 'Servicios',
		titulo : 'GFGFGFG'
	}
]
