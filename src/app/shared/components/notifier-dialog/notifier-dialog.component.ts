import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-notifier-dialog',
  templateUrl: './notifier-dialog.component.html',
  styleUrls: ['./notifier-dialog.component.css']
})
export class NotifierDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<NotifierDialogComponent>) { }

  ngOnInit() {
  }

  close(ok: boolean) {
    this.dialogRef.close(ok);
  }

}
