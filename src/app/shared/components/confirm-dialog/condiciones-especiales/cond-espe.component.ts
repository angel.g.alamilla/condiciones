import { Component, OnInit } from '@angular/core';
import { Tipo } from './tipo';
import { Condicion } from './condicion';
import { CondicionService } from './condicion.service';
import { Router, ActivatedRoute } from '@angular/router';
import { tap } from 'rxjs/operators';
import swal from 'sweetalert2';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-condespe',
  templateUrl: './cond-espe.component.html'
})
export class CondEspeComponent implements OnInit{

  private condicion: Condicion = new Condicion();
  private tipo: Tipo = null;

  tipos: Tipo[];
  condiciones: Condicion[];
  errores: string[];
  titulo: string = "Nueva Condición";

  constructor(private condicionService: CondicionService, private router: Router,
    private activatedRoute: ActivatedRoute, public dialog: MatDialog){ }

  ngOnInit(){
    this.condicionService.getTipos().subscribe(
      (tipos) => this.tipos = tipos
    );

    this.condicionService.getCondiciones().subscribe(
      (condiciones) => this.condiciones = condiciones
    );

  }

  create(): void {
    // this.condicion.id = 99;
    // this.condicion.descripcion = 'La que sea';
    // this.condicion.estatus = 99;
    // this.condicion.idGenerado = 'GGGG';
    this.condicion.texto = 'negritas';
    this.condicion.idGenerado = '';
    // this.condicion.tipo = '0123';
    // this.condicion.titulo = 'Condición Especial 99';
    console.log(this.tipo);
    this.condicion.tipo = this.tipo.nombre;

    console.log(this.condicion);
    this.condicionService.create(this.condicion)
      .subscribe(
        condicion => {
          this.router.navigate(['/condiciones-especiales']);

          swal.fire('Nueva condición', `La condición con ID: ${this.condicion.tipo} ha sido creado con éxito`, 'success');

          console.log(this.condicion);
          this.condicionService.getCondiciones().subscribe(
            (condiciones) => this.condiciones = condiciones
          );

        },
        err => {
          this.errores = err.error.errors as string[];
          console.error('Código del error desde el backend: ' + err.status);
          console.error(err.error.errors);
        }
      );
  }

  onOptionsSelected(event){
      this.condicion.idGenerado = event.target.value;
  }


  compararTipo(o1: Tipo, o2: Tipo): boolean {
  if (o1 === undefined && o2 === undefined) {
    return true;
  }

  return o1 === null || o2 === null || o1 === undefined || o2 === undefined ? false : o1.id === o2.id;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '250px'
    });

  }


}
