
export class Condicion{
  id: number;
  idGenerado: string;
  tipo: string;
  titulo: string;
  descripcion: string;
  texto: string;
  estatus: number;
}
