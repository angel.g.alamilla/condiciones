import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ModalComponent } from '../modal/modal.component';

import { Condicion } from './condicion';
import { CondicionService } from './condicion.service';
import { Tipo } from './tipo';

@Component({
  selector: 'app-condespe',
  templateUrl: './condi-espec.component.html'
})
export class CondiEspecComponent implements OnInit{

  private condicion: Condicion = new Condicion();
  private tipo: Tipo = null;

  tipos: Tipo[];
  condiciones: Condicion[];
  errores: string[];
  titulo: string = "Nueva Condición";

  constructor(private condicionService: CondicionService, private router: Router,
    private activatedRoute: ActivatedRoute, public dialog: MatDialog){ }

  ngOnInit(){
    this.condicionService.getTipos().subscribe(
      (tipos) => this.tipos = tipos
    );

    this.condicionService.getCondiciones().subscribe(
      (condiciones) => this.condiciones = condiciones
    );

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '250px'
    });
  }

}
