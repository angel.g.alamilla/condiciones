import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { UserComponent } from './user/user.component';
import { DirectivaComponent } from './directiva/directiva.component';
import { CondEspeComponent } from './condiciones-especiales/cond-espe.component';
import { CondicionService } from './condiciones-especiales/condicion.service';
import { UserService } from './user/user.service';
import { MaterialModule } from './material.module';
import { CondiEspecComponent } from './condiciones-especiales/condi-espec.component';
import { CondiEspecEditComponent } from './condiciones-especiales/condi-espec-edit.component';
import { MatIconModule, MatButtonModule } from '@angular/material';
import { GrandCuentComponent } from './grandes-cuentas/grand-cuent.component';
import { LicitacionComponent } from './licitaciones/licitacion.component';
import { GarantiaComponent } from './garantias/garantia.component';
import { LoginComponent } from './seguridad/login/login.component';
import { CondiParamComponent } from './condiciones-especiales/condi-param.component';
import { ConfirmDialogComponent } from './shared/components/confirm-dialog/confirm-dialog.component';
import { NotifierDialogComponent } from './shared/components/notifier-dialog/notifier-dialog.component';

const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: 'directivas', component: DirectivaComponent},
  { path: 'users', component: UserComponent },
  { path: 'condiciones-especiales', component: CondEspeComponent},
  { path: 'grandes-cuentas', component: GrandCuentComponent},
  { path: 'licitaciones', component: LicitacionComponent },
  { path: 'login', component: LoginComponent }
 ]

@NgModule({
  entryComponents: [
    CondiEspecComponent,
    CondiEspecEditComponent,
    CondiParamComponent,
    ConfirmDialogComponent
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    UserComponent,
    DirectivaComponent,
    CondEspeComponent,
    CondiEspecComponent,
    CondiEspecEditComponent,
    GrandCuentComponent,
    LicitacionComponent,
    GarantiaComponent,
    LoginComponent,
    CondiParamComponent,
    ConfirmDialogComponent,
    NotifierDialogComponent
  ],
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    MaterialModule,
    MatIconModule,
    MatButtonModule
  ],
  providers: [UserService, CondicionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
