import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Location } from '@angular/common';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
 })
export class HeaderComponent{
  titulo:string = 'Condiciones';
  title: Observable<string>;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private dialog: MatDialog,
    private router: Router,
  ) { }
    private location: Location

  goBack() {
    this.location.back();
  }

  cerrarSesion() {
    this.dialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: '"¿Está seguro de querer cerrar sesión en este momento?' }
    }).afterClosed().subscribe(
      res => {
        if (res) {
          console.log("Se cerro sesión")  ;
          this.router.navigate(['/']);          
        }
      }
    );
  }


}
