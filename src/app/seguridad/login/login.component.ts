import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  forma: FormGroup;
  user: string = "";
  pwd: string = "";

  constructor(    public formBuilder: FormBuilder) {
    this.forma = formBuilder.group({
      'user': this.user,
      'pwd' : this.pwd
    });

   }

  ngOnInit() {
  }

}
